﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSettings : MonoBehaviour {
    [Header("DO NOT APPLY MADE CHANGES TO PREFAB")]
    [Space]

    [SerializeField]
    float camMinXPos, camMinYPos, camMaxXPos, camMaxYPos;
    float temp, minX, maxX, minY, maxY, originalMinX, originalMaxX, originalMinY, originalMaxY;

    private void Start()
    {
        originalMinX = minX;
        originalMaxX = maxX;
        originalMinY = minY;
        originalMaxY = maxY;
    }

    private void LateUpdate()
    {

        minX = Mathf.Clamp(camMinXPos + Gamemanager.Camera.orthographicSize * Gamemanager.Camera.aspect, camMinXPos, maxX);
        maxX = Mathf.Clamp(camMaxXPos - Gamemanager.Camera.orthographicSize * Gamemanager.Camera.aspect, minX, camMaxXPos);
        minY = Mathf.Clamp(camMinYPos + Gamemanager.Camera.orthographicSize, camMinYPos, maxY);
        maxY = Mathf.Clamp(camMaxYPos - Gamemanager.Camera.orthographicSize, minY, camMaxYPos);







        /*
        Gamemanager.CamBoundsX = new Vector2(camMinXPos + Gamemanager.Camera.orthographicSize * Gamemanager.Camera.aspect, camMaxXPos - Gamemanager.Camera.orthographicSize * Gamemanager.Camera.aspect);
        Gamemanager.CamBoundsY = new Vector2(camMinYPos + Gamemanager.Camera.orthographicSize, camMaxYPos - Gamemanager.Camera.orthographicSize);
        */
  

        Gamemanager.CamBoundsX = new Vector2(minX, maxX);
        Gamemanager.CamBoundsY = new Vector2(minY, maxY);


    }
    void Swap(float first, float second) {
        float temp = second;
        first = second;
        second = temp;
    }
}
