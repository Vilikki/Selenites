﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DollSpawner : MonoBehaviour {

    private Transform target;      // Player position
    private Transform myTransform; // Spawner position
    public GameObject enemy;       // The enemy object that is spawned
    public int minDistance;        // The minimum distance the player should be from the spawner
    private int maxEnemies;        // The max amount of enemies to spawn per wave
    private int absoluteMax;       // So things don't get out of hand
    private int enemiesSpawned;    // Enemies spawned during this wave
    private float timer;
    public float limit = 1f;       // The amount of time in seconds the spawner waits between each spawned enemy
    public bool increaseEnemies;
    
    [SerializeField, Tooltip("The time to wait when a wave ends before starting next wave")]
    private float waveWait;
    private float waveTimer;
    private bool newWave;          // Boolean to check if the amount of maxEnemies should be increased
    private int waveCounter;
    private int everyOther;        // To increase the amount of enemies every other wave

    Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("Spawn", true);
        

    }
    void Start()
    {
        
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        target = player.transform;
        myTransform = transform;
        maxEnemies = 1;
        absoluteMax = 30;
        StartCoroutine(Spawn());
        //Instantiate(enemy, transform.position, Quaternion.identity);
    }

    // WaveTimer determines how long the spawner waits between each spawn
    // MaxEnemies determines how many enemies to spawn at a time
    // The timer spaces out the spawning so that the enemies are not instantiated as a pile
    void Update()
    {
        if (newWave)
        {
            //Debug.Log("New wave");
            
            if (everyOther == 2)
            {
                if (increaseEnemies)
                {
                    maxEnemies++;
                }
                everyOther = 0;
            }

            waveWait += 2;
            limit++;
            newWave = false;
        }
        
        waveTimer += Time.deltaTime;

        if (waveTimer >= waveWait)
        {
            timer += Time.deltaTime;

            if ((Vector3.Distance(target.position, myTransform.position) > minDistance) && timer >= limit)
            {
                
                if (GameObject.FindGameObjectsWithTag("Enemy").Length < absoluteMax)
                {
                    anim.SetBool("Spawn", true);
                    //Instantiate(enemy, transform.position, Quaternion.identity);
                    StartCoroutine(Spawn());
                }
                enemiesSpawned++;
                timer = 0f;
            }
            
        }

        

        if (enemiesSpawned == maxEnemies)
        {
            //Debug.Log("End spawning dolls");
            waveTimer = 0;
            newWave = true;
            enemiesSpawned = 0;
            everyOther++;
            
        }
    }
    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(1);
        Instantiate(enemy, transform.position, Quaternion.identity);
        anim.SetBool("Spawn", false);
    }
}
