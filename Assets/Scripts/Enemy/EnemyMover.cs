﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : EnemyController {
	
    // Speed of the enemy movement
	public float speed;
    // The distance to the player at which the enemy stops trying to get closer
    public float stoppingDistance;
    public bool lookRight = true;
    private GameObject[] startNodes;
    private GameObject[] endNodes;
    private GameObject closestNode = null;
    private Transform target;
    private Vector3 position;
    private bool touchingObstacle;
    private Timer timer;
    private bool goToEnd = false;
    // enemy rigid body
    // Rigidbody2D erb;

    // Use this for initialization
    void Start () {
        timer = GetComponent<Timer>();
        position = transform.position;
        //target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        target = Gamemanager.PlayerTransform.GetChild(0).transform;
        // erb = GetComponent<Rigidbody2D>();
        if (startNodes == null)
        {
            startNodes = GameObject.FindGameObjectsWithTag("StartNode");
        }
        if (endNodes == null)
        {
            endNodes = GameObject.FindGameObjectsWithTag("EndNode");
        }
    }
	
	// Update is called once per frame
	void Update () {
        position = transform.position;
        SetTarget();
        
        // Moves the enemy towards te player until stoppingDistance is reached
        if (Vector2.Distance(transform.position, target.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        

        // if enemy is on player's left side, flip the sprite to look right and vice versa
        if ((transform.position.x < target.position.x && !lookRight) || (transform.position.x > target.position.x && lookRight))
        {
            Flip();
        }

    }

    // Flip the sprite of the enemy
    void Flip()
    {
        lookRight = !lookRight;
        Vector3 enemyScale = transform.localScale;
        enemyScale.x *= -1;
        transform.localScale = enemyScale;
    }


    void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag == "obstacle")
        {
            timer.StartTimer(3);
        }
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        
             
            if (collision.gameObject.tag == "obstacle" && timer.CurrentTime >= timer.MaxTime)
            {
                touchingObstacle = true;

            }
        
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        
        if (collision.gameObject.tag == "obstacle")
        {
            
            touchingObstacle = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "StartNode")
        {
            
            if (target.gameObject.tag == "StartNode")
            {
                goToEnd = true;
                timer.StopTimer();
            }
        }
        else if(collision.gameObject.tag == "EndNode")
        {
            goToEnd = false;
        }
    }

    private void SetTarget()
    {
        float distance = Mathf.Infinity;
        
        if (touchingObstacle && !goToEnd)
        {
            foreach (GameObject node in startNodes)
            {
                Vector3 diff = node.transform.position - position;
                float curDistance = diff.sqrMagnitude;
                if (curDistance < distance)
                {
                    closestNode = node;
                    distance = curDistance;
                }
            }
            target = closestNode.GetComponent<Transform>();
            
        } else if (goToEnd)
        {
            
            foreach (GameObject node in endNodes)
            {
                Vector3 diff = node.transform.position - position;
                float curDistance = diff.sqrMagnitude;
                //Debug.Log(node.name);
                
                if (curDistance < distance)
                {
                    closestNode = node;
                    distance = curDistance;
                }
            }
            target = closestNode.GetComponent<Transform>();

        }
        else if(target.gameObject.tag != "StartNode") 
        {
            target = Gamemanager.PlayerTransform.GetChild(0).transform;

        }
        //Debug.Log(target.name); 
    }
    


}
