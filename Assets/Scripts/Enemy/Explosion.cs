﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    public Vector2 minRadius = new Vector2(0.5f, 0.5f);
    public Vector2 maxRadius = new Vector2(10, 10);
    public float timer = 1;
    public bool explode;

    private void Awake()
    {
        gameObject.SetActive(false);
        explode = false;
        transform.localScale = minRadius;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        transform.localScale = Vector3.Lerp(transform.localScale, maxRadius, Time.deltaTime * 5);
        if (timer >= 1)
        {
            Destroy(gameObject);
        }
        
    }

    /*void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy" )
        {
            Destroy(other.gameObject);
        }
    }*/

}
