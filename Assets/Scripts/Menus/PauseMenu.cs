﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;


public class PauseMenu : MonoBehaviour {

    public EventSystem es;
    [SerializeField]
    GameObject continueButton, menuButton, pauseMenu;
    

    private void Awake()
    {
        //es.firstSelectedGameObject = continueButton;

    }

    private void Update()
    {
        if (es.currentSelectedGameObject == null && Input.GetAxis("Vertical") != 0) {
            es.SetSelectedGameObject(continueButton);
        }

        if (Input.GetButtonDown("Cancel"))
        {
            Continue();
        }
    }

    public void Continue()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("Main_Menu");
    }
}
