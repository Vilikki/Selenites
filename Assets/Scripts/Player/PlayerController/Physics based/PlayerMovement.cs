﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float speed;
    public float jumpPower = 15;
    float upwardSlideMomentum;
    Vector2 dir;
    Vector2 jumpDir;

    Vector3 speedRef = Vector3.zero;

    Animator anim;
    SpriteRenderer spriteRend;

    Rigidbody2D rb;

    public bool onGround = false;
    public LayerMask groundLayer;
    int maxJumpAmount = 1;
    public int jumpsLeft;

    RaycastHit2D groundCheck;

    private float buttonCooler = 0.5f;
    private int buttonCounter = 0;
    private bool hasDoubleTapped = false;
    private bool isDashing = false;
    private int rightCounter = 0;
    private int leftCounter = 0;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();

        anim = GetComponentInChildren<Animator>();

        jumpsLeft = maxJumpAmount;

        spriteRend = GetComponentInChildren<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.DrawRay(transform.position, Vector2.left);
        Debug.DrawRay(transform.position, Vector2.down * .1f, Color.red);

        

    }

    public void Move()
    {


        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            rightCounter++;
            leftCounter = 0;
            Debug.Log("rightCounter: " + rightCounter);
        } else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            leftCounter++;
            rightCounter = 0;
            Debug.Log("leftCounter: " + leftCounter);
        }

        if (Input.GetButtonDown("Horizontal") )
        {

            if (buttonCooler > 0 && buttonCounter == 1 && (leftCounter > 1 || rightCounter > 1))
            {
                hasDoubleTapped = true;
            }
            else
            {
                buttonCooler = 0.5f;
                buttonCounter += 1;
            }
        }

        if (buttonCooler > 0)
        {

            buttonCooler -= 1 * Time.deltaTime;

        }
        else
        {
            buttonCounter = 0;
            hasDoubleTapped = false;
        }


        if(hasDoubleTapped == false)
        {
            dir = new Vector2(Input.GetAxisRaw("Horizontal") * speed, rb.velocity.y);
        }
        else
        {
            dir = new Vector2(Input.GetAxisRaw("Horizontal") * (speed * 3), rb.velocity.y);
            rightCounter = 0;
            leftCounter = 0;
        }



        if(dir.x != 0) rb.velocity = Vector3.SmoothDamp(rb.velocity, dir, ref speedRef, .1f);

        

        groundCheck = Physics2D.Raycast(transform.position, Vector2.down, .05f, groundLayer);
        if (groundCheck.collider != null)
        {
            onGround = true;
        }
        else {
            onGround = false;
        }

        if (onGround)
        {
            jumpsLeft = maxJumpAmount;
            jumpDir = new Vector2(0, jumpPower);
            anim.SetBool("OnGround", true);
            anim.SetBool("Falling", false);
            anim.SetBool("Jump", false);
        }
        else {
            anim.SetBool("OnGround", false);
            anim.SetBool("Running", false);
            if (rb.velocity.y < 0) {
                anim.SetBool("Falling", true);
                anim.SetBool("Jump", false);
            }
        }

        if (dir.x != 0 && onGround)
        {
            anim.SetBool("Running", true);
        }
        else {
            anim.SetBool("Running", false);
        }

        if (rb.velocity.x < 0) spriteRend.flipX = true;
        else if(rb.velocity.x > 0) spriteRend.flipX = false;


    }

    public void Jump()
    {
        if (!onGround) rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(jumpDir, ForceMode2D.Impulse);
        anim.SetBool("Jump", true);
        jumpsLeft--;
    }

        
    

    private void OnCollisionStay2D(Collision2D collision)
    {
        

        

        if (collision.gameObject.layer == 8)
        {
            if (Input.GetButtonDown("Jump") && jumpsLeft > 0 && !onGround)
            {


                if (collision.gameObject.transform.position.x < transform.position.x)
                {
                    jumpDir = new Vector2(jumpPower, jumpPower);
                }
                else if (collision.gameObject.transform.position.x > transform.position.x)
                {
                    jumpDir = new Vector2(-jumpPower, jumpPower);
                }
                else {
                    jumpDir = new Vector2(0, jumpPower);
                }

                rb.velocity = Vector2.zero;
                //Jump();

            }
        }

        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        
        //check if player is touching a wall for upward slide
        if (collision.gameObject.layer == 8)
        {
            jumpsLeft = maxJumpAmount;
            //slide up walls while going upwards on collision
            if (rb.velocity.y >= 0 && !onGround)
            {
                upwardSlideMomentum = Mathf.Sqrt(Mathf.Pow(rb.velocity.x, 2) + Mathf.Pow(rb.velocity.y, 2)); 
                rb.velocity = new Vector2(rb.velocity.x / 2, rb.velocity.y / 2 + upwardSlideMomentum);
            }
        }

          
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //if (collision.gameObject.layer == 8) onGround = false;
    }

}
*/