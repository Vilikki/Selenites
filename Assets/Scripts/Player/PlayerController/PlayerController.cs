﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    //PlayerMovement movement;
    Movement movement;
    public GameObject shield, target, cursor, gameOverContainer, pauseContainer;
    Health playerHealth;
    GameObject cursorSprite;
    [SerializeField]
    GameObject shieldExplosion;

    [SerializeField]
    GameObject explosionPrefab;

    Rigidbody2D shieldRb, rb;
    SpriteRenderer srend;
    Collider2D[] playerColliders;
    Animator anim;

    GameOverMenu gameOver;
    bool shieldShot = false;
    //private bool gotHit = false; // a bool for whether or not the player has hit an enemy

    public GameObject[] blasts;

    [SerializeField]
    float aimPointOffset = 2f;

    private bool isDead = false;
    private float levelReloadTimer = 2.5f;
    private float immortalTimer;
    private Renderer rend;
    float flashTimer, aimResetTimer = 0;

    float shieldSpeed = 15f;

    Vector3 aimVector, mousePos, lastFrameMousePos;
    bool mouseInUse = true;

    [SerializeField]
    Camera cam;

    private AudioSource sound;
    [SerializeField]
    private AudioClip hitSound;
    [SerializeField]
    private AudioClip throwSound;
    [SerializeField]
    private AudioClip explosion;


    //Animator outline;

    private void Awake()
    {
        //outline = GetComponentInChildren<OutlineHealthbar>().gameObject.GetComponent<Animator>();
        anim = GetComponentInChildren<Animator>();
        //movement = GetComponent<PlayerMovement>();
        movement = GetComponent<Movement>();
        shieldRb = shield.GetComponent<Rigidbody2D>();
        rb = GetComponent<Rigidbody2D>();
        playerColliders = GetComponents<Collider2D>();
        playerHealth = GetComponent<Health>();
        srend = GetComponent<SpriteRenderer>();
        sound = GetComponent<AudioSource>();
        Gamemanager.PlayerHealth = playerHealth;
        Gamemanager.PlayerTransform = GetComponent<Transform>();
        Gamemanager.PlayerDead = isDead;
        cursorSprite = cursor.transform.GetChild(0).gameObject;
        shieldExplosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
        shieldExplosion.transform.parent = transform.root;
        shieldExplosion.SetActive(false);

    }

    // Use this for initialization
    void Start() {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
        

        Time.timeScale = 1;
        foreach (Collider2D c in playerColliders)
        {
            foreach (GameObject b in blasts)
            {
                if(b != null) Physics2D.IgnoreCollision(c, b.GetComponent<Collider2D>());
            }
        }
        
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (isDead == false) {
            movement.Move();
        }
        else
        {
            levelReloadTimer -= Time.deltaTime;
        }

        if (levelReloadTimer <= 0)
        {
            gameOverContainer.SetActive(true);
            Time.timeScale = 0;
        }


    }
    private void Update()
    {

        if(Gamemanager.PlayerDead != isDead) Gamemanager.PlayerDead = isDead;

        //Call MouseAim which moves the cursor object to mouse position
        if (!isDead && Time.timeScale == 1) AimProjectile();

        
        if (Input.GetButtonDown("Fire2") && !isDead && Time.timeScale == 1)
        {
            ShootShield();
        }

        if(Input.GetButtonDown("Fire3") && !isDead && Time.timeScale == 1)
        {
            ReturnShield();
        }
        

        ImmortalFlash();

        isDead = playerHealth.IsDead();
        if (isDead) anim.SetBool("Die", true);

        if (Input.GetButtonDown("Pause") && Time.timeScale != 0)
        {
            pauseContainer.SetActive(true);
            Time.timeScale = 0;
        }



    }

    void AimProjectile() {

        

        aimVector = new Vector2(Input.GetAxis("ContRStickX"), Input.GetAxis("ContRStickY"));
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition) - target.transform.position;

   

        if (aimVector != Vector3.zero)
        {
            mouseInUse = false;
        }

        else if (new Vector2(Input.GetAxis("MouseX"), Input.GetAxis("MouseY")) != Vector2.zero)
        {
            mouseInUse = true;
        }

        if (mouseInUse) aimVector = mousePos;
        cursor.transform.position = target.transform.position + aimVector.normalized * aimPointOffset + Vector3.forward * 10;
        float angle = Mathf.Atan2(aimVector.x, aimVector.y) * Mathf.Rad2Deg;
        cursorSprite.transform.rotation = Quaternion.AngleAxis(-angle, Vector3.forward);


        if (isDead || (!mouseInUse && aimVector == Vector3.zero)) cursor.gameObject.SetActive(false);
        else if (!cursor.gameObject.activeSelf) cursor.gameObject.SetActive(true);
    }

    void ShootShield() {

        //if shield hasn't been shot, shoot it towards current mouse position
        if (!shieldShot)
        {
            sound.PlayOneShot(throwSound);
            Vector2 dir = cursor.transform.position - target.transform.position;
            dir.Normalize();
            if (dir == Vector2.zero && !srend.flipX)
            {
                dir = Vector2.right;
            }
            else if (dir == Vector2.zero && srend.flipX) {
                dir = Vector2.left;
            }
            //Coroutine for ignoring the player colliders when shooting
            StartCoroutine(ShieldUtil());

            //shield rigidbody has gravity scale of 0, meaning it floats and never falls downwards on its own
            //shield is shot with basic addforce functionality
            shieldRb.AddForce(dir * shieldSpeed, ForceMode2D.Impulse);
            anim.SetTrigger("Throw");
            //outline.SetTrigger("Throw");
            //set checkboolean to true for changed behavior from shooting the shield to returning it towards player
            shieldShot = true;
        }
        
        else {

            ExplodeShield();
            
        }

    }
    void ExplodeShield()
    {
        if (shieldShot)
        {
            sound.PlayOneShot(explosion);
            shieldExplosion.transform.position = shield.transform.position;
            if (shieldExplosion.activeSelf) shieldExplosion.SetActive(false);
            shieldExplosion.SetActive(true);

            shield.SetActive(false);
            rb.velocity = Vector2.zero;
            shieldShot = false;
        }
    }

    void ReturnShield() {
        if (shieldShot) {
            //Shield returns towards player, 
            float speed;
            speed = Mathf.Sqrt(Mathf.Pow(shieldRb.velocity.x, 2) + Mathf.Pow(shieldRb.velocity.y, 2));

            //on input while the shield is already shot, stop its current movement...
            shieldRb.velocity = Vector2.zero;

            //... then get the difference between the projectile and player's positions and normalize the new direction for multiplication with speed
            Vector2 targetPos = target.transform.position - shieldRb.transform.position;
            targetPos.Normalize();

            //shoot the shield in the new direction at the defined speed
            shieldRb.AddForce(targetPos * speed, ForceMode2D.Impulse);
        }
    }

    //Coroutine for collision ignoring, might be unnecessarily complicated
    private IEnumerator ShieldUtil() {

        foreach (Collider2D coll in playerColliders)
        {
            Physics2D.IgnoreCollision(shield.GetComponent<Collider2D>(), coll);
        }
        shield.transform.position = target.transform.position;
        shield.SetActive(true);

        //shieldRetrievable = false;

        yield return new WaitForSeconds(1f);

        foreach (Collider2D coll in playerColliders)
        {
            Physics2D.IgnoreCollision(shield.GetComponent<Collider2D>(), coll, false);
        }

        //shieldRetrievable = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("ShieldProjectile"))
        {
            shield.SetActive(false);
            rb.velocity = Vector2.zero;
            shieldShot = false;
            if (!movement.Dashing) {
                Gamemanager.ShieldBounces = 0;
            }
            
        }
        if(collision.gameObject.tag == "HealthPickup")
        {
            
            playerHealth.Heal(20);
        }
        if (collision.gameObject.tag.Equals("Enemy") == true)
        {
            playerHealth.TakeDamage(20);
            if (!playerHealth.IsImmortal && !isDead)
            {
                sound.PlayOneShot(hitSound);
            }
            
            playerHealth.setImmortal(true);
            if (immortalTimer <= 0)
            {
                immortalTimer = 1;

            }

            isDead = playerHealth.IsDead();
            if (isDead == true)
            {
                anim.SetBool("Die", true);

            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        

        // Checks if the player hit an enemy
        if (collision.gameObject.tag.Equals("Enemy") == true)
        {
            playerHealth.TakeDamage(20);
            if (!playerHealth.IsImmortal && !isDead)
            {
                sound.PlayOneShot(hitSound);
            }
            //gotHit = true;
            // if hit player takes no damage for 1 second
            playerHealth.setImmortal(true);
            if (immortalTimer <= 0)
            {
                immortalTimer = 1;
                
            }

            if (movement.jumpsLeft == 0) {
                movement.jumpsLeft = 1;
            }
           
            isDead = playerHealth.IsDead();
            if(isDead == true) {
                
                anim.SetBool("Die", true);

            }
        }
    }

    //when hit by enemy, player is immortal for a while and flashes
    public void ImmortalFlash()
    {
        if (immortalTimer > 0)
        {
            immortalTimer -= Time.deltaTime;
        }
        if (immortalTimer <= 0)
        {
            playerHealth.setImmortal(false);
        }

        rend.enabled = true;
        if (flashTimer <= 0 && immortalTimer > 0 && isDead == false)
        {
            flashTimer = 0.1f;
        }
        flashTimer -= Time.deltaTime;
        if (flashTimer > 0)
        {
            rend.enabled = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "HealthPickup")
        {

            playerHealth.Heal(20);
        }
        if (other.gameObject.tag == "Explosion")
        {
            playerHealth.TakeDamage(40);
            if (!playerHealth.IsImmortal && !isDead) {
                sound.PlayOneShot(hitSound);
            }

            // if hit player takes no damage for 1 second
            playerHealth.setImmortal(true);
            if (immortalTimer <= 0)
            {
                immortalTimer = 1;

            }

            isDead = playerHealth.IsDead();
            if (isDead == true)
            {
                anim.SetBool("Die", true);

            }
        }
    }

    
   
}
