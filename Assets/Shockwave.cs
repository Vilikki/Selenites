﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shockwave : MonoBehaviour {
    [SerializeField]
    SpriteRenderer srend;
    [SerializeField]
    Rigidbody2D rb;
    Collider2D collider;


    int dir = 1;
    [SerializeField]
    LayerMask ground;
    float timer = 0;
    [SerializeField]
    float activeTimeLimit = 5;
    [SerializeField, Tooltip("Used for deactivating the wave if it's about to go over the edge of the platform it's currently traveling along")]
    float offset = .55f;
    [SerializeField]
    float speedMultiplier = 1;

    [SerializeField]
    Color hostileColor, friendlyColor;
    bool Hostile;
    float timeActive = 0;

	// Use this for initialization
	void Awake () {

        srend = GetComponentInChildren<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
	}
    private void Update()
    {
        if(timeActive < 2) timeActive += Time.deltaTime;
    }
    private void FixedUpdate()
    {
        if(gameObject.activeSelf) Move();
        if (gameObject.activeSelf && timer >= activeTimeLimit)
        {
            Disable();
        }
    }

    public void Activate(bool hostile) {
        Hostile = hostile;
        gameObject.SetActive(true);

        if (hostile)
        {
            srend.color = hostileColor;
            gameObject.layer = 9;
        }
        else
        {
            srend.color = friendlyColor;
            gameObject.layer = 8;
        }
        //gameObject.layer = 8;
    }

    public void Disable()
    {
        gameObject.SetActive(false);
        transform.position = transform.parent.position;
        timer = 0;
        timeActive = 0;
    }

    public void Move()
    {
        timer += Time.deltaTime;
        rb.MovePosition(transform.position + transform.right * dir * speedMultiplier * Time.deltaTime);
        if (timeActive >= .25f) {
            RaycastHit2D hitFront = Physics2D.Raycast(transform.position, transform.right * dir, .5f, ground);
            RaycastHit2D hitDown = Physics2D.Raycast(transform.position + transform.right * dir * (dir * offset), -transform.up, .5f, ground);

            if (hitFront.collider != null || hitDown.collider == null)
            {
                Disable();
            }
        }
        
    }
    /// <summary>
    /// Sets the right / left direction of the wave
    /// </summary>
    /// <param name="dir"> -1 = left, 1 = right, 0 = stationary</param>
    public void SetDirection(int dir)
    {
        this.dir = dir;
        if (dir < 0) srend.flipX = true;
        else srend.flipX = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (Hostile && collision.gameObject.layer == 10) {
            Gamemanager.PlayerHealth.TakeDamage(10);
            Disable();
        }
    }

}
